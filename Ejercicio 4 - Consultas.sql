﻿USE ciclistas;

/* Modulo 1 Unidad 2 */

/* Consulta de seleccion 4 */

/* Consulta 1 */
  -- C1
  SELECT DISTINCT e.dorsal FROM etapa e;
  -- Final
  SELECT c.nombre,c.edad FROM ciclista c INNER JOIN (SELECT DISTINCT e.dorsal FROM etapa e) c1 ON c.dorsal= c1.dorsal;


/* Consulta 2 */
  -- C1
  SELECT DISTINCT p.dorsal FROM puerto p;
  -- Final
  SELECT c.nombre,c.edad FROM ciclista c INNER JOIN (SELECT DISTINCT p.dorsal FROM puerto p) c1 ON c.dorsal= c1.dorsal;
/* Consulta 3 */
  -- C1
  SELECT DISTINCT e.dorsal FROM etapa e;
  -- C2
  SELECT DISTINCT p.dorsal FROM puerto p;
  -- Final
  SELECT c.nombre,c.edad 
    FROM ciclista c 
    INNER JOIN (SELECT DISTINCT e.dorsal FROM etapa e) c1 ON c.dorsal=c1.dorsal
    INNER JOIN (SELECT DISTINCT p.dorsal FROM puerto p) c2 ON c.dorsal=c2.dorsal;

/* Consulta 4 */
  SELECT DISTINCT e.director 
    FROM equipo e 
    INNER JOIN ciclista c ON e.nomequipo = c.nomequipo
    INNER JOIN etapa e1 ON c.dorsal = e1.dorsal;

/* Consulta 5 */
  SELECT DISTINCT c.dorsal,c.nombre FROM ciclista c INNER JOIN lleva l ON c.dorsal = l.dorsal;

/* Consulta 6 */
  -- C1
  SELECT m.código FROM maillot m WHERE m.color='amarillo';
  
  SELECT DISTINCT c.dorsal,c.nombre 
    FROM ciclista c 
    INNER JOIN lleva l ON c.dorsal = l.dorsal
    INNER JOIN (SELECT m.código FROM maillot m WHERE m.color='amarillo') c1 ON l.código=c1.código;

/* Consulta 7 */
  SELECT DISTINCT l.dorsal FROM lleva l INNER JOIN equipo e ON l.dorsal;

/* Consulta 8 */
  SELECT DISTINCT p.numetapa FROM puerto p;

/* Consulta 9 */
  -- C1
  SELECT c.dorsal FROM ciclista c WHERE c.nomequipo='Banesto';

  -- Final
  SELECT DISTINCT e.numetapa,e.kms
    FROM (SELECT c.dorsal FROM ciclista c WHERE c.nomequipo='Banesto') c1
    INNER JOIN etapa e ON c1.dorsal=e.dorsal
    INNER JOIN puerto p ON e.numetapa = p.numetapa;

/* Consulta 10 */
  SELECT COUNT(DISTINCT e.dorsal) FROM etapa e JOIN puerto p ON e.numetapa = p.numetapa;

/* Consulta 11 */
  -- C1
  SELECT c.dorsal FROM ciclista c WHERE c.nomequipo='Banesto';
  -- Final
  SELECT p.nompuerto
    FROM puerto p 
    INNER JOIN 
      (SELECT c.dorsal FROM ciclista c WHERE c.nomequipo='Banesto') c1 ON p.dorsal=c1.dorsal;

/* Consulta 12 */
SELECT DISTINCT c2.numetapa FROM puerto p 
  JOIN (SELECT c.dorsal 
          FROM ciclista c 
          WHERE c.nomequipo='Banesto') c1 ON c1.dorsal=p.dorsal
  JOIN (SELECT e.numetapa 
          FROM etapa e 
          WHERE e.kms>=200) c2 ON c2.numetapa=p.numetapa;